import { playGame } from './utils/playGame'

const main = () => {
  const first = playGame()
  const second = playGame(true)

  console.log({ first, second })
}

main()
