/**
 * Game Type:\
 * 0 - mana spent\
 * 1 - Player MP\
 * 2 - Player HP\
 * 3 - Boss HP\
 * 4 - armor effect turns\
 * 5 - damage effect turns\
 * 6 - mana effect turns
 */
export type Game = [number, number, number, number, number, number, number]

export type Effect = 'Shield' | 'Poison' | 'Recharge'
export type Spell = 'Missile' | 'Drain' | Effect
