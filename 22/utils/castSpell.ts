import { Game, Spell } from '../types'

/*
Magic Missile costs 53 mana. It instantly does 4 damage.
Drain costs 73 mana. It instantly does 2 damage and heals you for 2 hit points.
Shield costs 113 mana. It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
Poison costs 173 mana. It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
Recharge costs 229 mana. It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.
 */

export const castSpell = (game: Game, spell: Spell) => {
  switch (spell) {
    case 'Missile':
      game[0] += 53
      game[1] -= 53
      game[3] -= 4
      break
    case 'Drain':
      game[0] += 73
      game[1] -= 73
      game[2] += 2
      game[3] -= 2
      break
    case 'Shield':
      if (game[4]) {
        return 'LOSS'
      }

      game[0] += 113
      game[1] -= 113
      game[4] = 6
      break
    case 'Poison':
      if (game[5]) {
        return 'LOSS'
      }

      game[0] += 173
      game[1] -= 173
      game[5] = 6
      break
    case 'Recharge':
      if (game[6]) {
        return 'LOSS'
      }

      game[0] += 229
      game[1] -= 229
      game[6] = 5
  }
}
