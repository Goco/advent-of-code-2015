import { Game } from '../types'

export const applyEffects = (game: Game) => {
  const armorEffect = game[4]
  const damageEffect = game[5]
  const manaEffect = game[6]

  if (armorEffect) {
    game[4] -= 1
  }
  if (damageEffect) {
    game[3] -= 3
    game[5] -= 1
  }
  if (manaEffect) {
    game[1] += 101
    game[6] -= 1
  }
}
