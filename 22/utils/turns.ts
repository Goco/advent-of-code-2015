import { Game, Spell } from '../types'
import { applyEffects } from './applyEffects'
import { checkGame } from './checkGame'
import { BOSS_DMG, SHIELD_ARMOR } from '../constants'
import { castSpell } from './castSpell'

export const playerTurn = (game: Game, spell: Spell, isHard = false) => {
  if (isHard) {
    game[2] -= 1
    const checkHard = checkGame(game)
    if (checkHard) {
      return checkHard
    }
  }

  // Effects
  applyEffects(game)
  const check1 = checkGame(game)
  if (check1) {
    return check1
  }

  //Cast Spell
  const check2 = castSpell(game, spell)
  if (check2) {
    return check2
  }

  const check3 = checkGame(game)
  if (check3) {
    return check3
  }
}

export const bossTurn = (game: Game) => {
  // Effects
  applyEffects(game)
  const check1 = checkGame(game)
  if (check1) {
    return check1
  }

  // Boss attack
  const armorEffect = game[4]
  const attack = BOSS_DMG - (armorEffect ? SHIELD_ARMOR : 0)
  game[2] -= attack
  const check2 = checkGame(game)
  if (check2) {
    return check2
  }
}
