import { Game } from '../types'
import { INITIAL_GAME, SPELLS } from '../constants'
import { bossTurn, playerTurn } from './turns'

const hash = (game: Game) => game.join('-')

export const playGame = (isHard = false) => {
  const games: Game[] = [[...INITIAL_GAME]]
  const processedGames: Set<string> = new Set()

  let winningGame: Game | undefined = undefined
  while (!winningGame) {
    const game = games.shift()
    if (!game) {
      break
    }

    const newGames = SPELLS.flatMap((spell) => {
      if (winningGame) {
        return []
      }
      const newGame: Game = [...game]

      // Player
      const playerCheck = playerTurn(newGame, spell, isHard)
      if (playerCheck) {
        if (playerCheck === 'WIN') {
          winningGame = [...newGame]
        }
        return []
      }

      // Boss
      const bossCheck = bossTurn(newGame)
      if (bossCheck) {
        if (bossCheck === 'WIN') {
          winningGame = [...newGame]
        }
        return []
      }

      return [newGame]
    }).filter((newGame) => !processedGames.has(hash(newGame)))

    newGames.forEach((newGame) => processedGames.add(hash(newGame)))
    games.push(...newGames)
    games.sort((a, b) => a[0] - b[0])
  }

  return winningGame?.[0]
}
