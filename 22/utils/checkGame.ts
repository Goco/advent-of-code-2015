import { Game } from '../types'

export const checkGame = (game: Game) => {
  const playerMP = game[1]
  const playerHP = game[2]
  const bossHP = game[3]

  if (playerMP <= 0 || playerHP <= 0) {
    return 'LOSS'
  }
  if (bossHP <= 0) {
    return 'WIN'
  }
}
