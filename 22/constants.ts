import { Game, Spell } from './types'

// Player
export const PLAYER_HP = 50
export const PLAYER_MP = 500
export const SHIELD_ARMOR = 7

// Boss
export const BOSS_HP = 58
export const BOSS_DMG = 9

// Initial game
export const INITIAL_GAME: Game = [0, PLAYER_MP, PLAYER_HP, BOSS_HP, 0, 0, 0]

// Spells
export const SPELLS: Spell[] = ['Missile', 'Drain', 'Shield', 'Poison', 'Recharge']
