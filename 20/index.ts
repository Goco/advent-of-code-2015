const INPUT = 36000000

const firstPart = () => {
  const size = Math.floor(INPUT / 10)
  const houses: number[] = Array(size).fill(0)

  for (let i = 1; i < size; ++i) {
    for (let j = i; j < size; j += i) {
      houses[j] += i * 10
    }
  }

  let result = 0
  houses.some((points, house) => {
    if (points >= INPUT) {
      result = house
      return true
    }
  })

  return result
}

const secondPart = () => {
  const size = Math.floor(INPUT / 11)
  const maxVisits = 50
  const houses: number[] = Array(size).fill(0)

  for (let i = 1; i < size; ++i) {
    let visitedCount = 0
    for (let j = i; j < size; j += i) {
      ++visitedCount
      houses[j] += i * 11
      if (visitedCount >= maxVisits) {
        break
      }
    }
  }

  let result = 0
  houses.some((points, house) => {
    if (points >= INPUT) {
      result = house
      return true
    }
  })

  return result
}

const main = () => {
  const first = firstPart()
  const second = secondPart()

  console.log({ first, second })
}

main()
