import { readInput } from '../utils/readInput'
import { Grid } from './types'
import { simulateGrid } from './utils/simulateGrid'
import { getGridFromLines } from './utils/getGridFromLines'

const firstPart = (grid: Grid): number => {
  for (let i = 0; i < 100; ++i) {
    grid = simulateGrid(grid)
  }

  return grid.flatMap((line) => line).filter((s) => s === '#').length
}

const secondPart = (grid: Grid): number => {
  const X = grid.length
  const Y = grid[0].length

  grid[0][0] = '#'
  grid[X - 1][0] = '#'
  grid[0][Y - 1] = '#'
  grid[X - 1][Y - 1] = '#'

  for (let i = 0; i < 100; ++i) {
    grid = simulateGrid(grid, true)
  }

  return grid.flatMap((line) => line).filter((s) => s === '#').length
}

const main = () => {
  const lines = readInput()
  let grid = getGridFromLines(lines)
  const first = firstPart(grid)

  let grid2 = getGridFromLines(lines)
  const second = secondPart(grid2)

  console.log('First part:', first)
  console.log('Second part:', second)
}

main()
