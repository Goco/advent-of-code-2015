import { Grid } from '../types'

export const getGridFromLines = (lines: string[]): Grid => lines.map((line) => line.split('')) as Grid