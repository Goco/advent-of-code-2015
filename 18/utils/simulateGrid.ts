import { Grid } from '../types'

export const simulateGrid = (grid: Grid, withCorners = false): Grid => {
  const X = grid.length
  const Y = grid[0].length

  const newGrid = Array.from({ length: X }).map(() => Array.from({ length: Y }).fill('.')) as Grid

  for (let x = 0; x < X; ++x) {
    for (let y = 0; y < Y; ++y) {
      let neighbors = 0
      for (let i = Math.max(0, x - 1); i <= Math.min(X - 1, x + 1); ++i) {
        for (let j = Math.max(0, y - 1); j <= Math.min(Y - 1, y + 1); ++j) {
          neighbors += grid[i][j] === '#' ? 1 : 0
        }
      }

      if (grid[x][y] === '#') {
        if (neighbors === 3 || neighbors === 4) {
          newGrid[x][y] = '#'
        }
      } else {
        if (neighbors === 3) {
          newGrid[x][y] = '#'
        }
      }
    }
  }

  if (withCorners) {
    newGrid[0][0] = '#'
    newGrid[X - 1][0] = '#'
    newGrid[0][Y - 1] = '#'
    newGrid[X - 1][Y - 1] = '#'
  }

  return newGrid
}