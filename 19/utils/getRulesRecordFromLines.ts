import { RulesRecord } from '../types'

export const getRulesRecordFromLines = (lines: string[]): RulesRecord =>
  lines.reduce((acc, line) => {
    const [key, value] = line.split(' => ')
    acc[key] = acc[key] ?? []
    acc[key].push(value)

    return acc
  }, {})