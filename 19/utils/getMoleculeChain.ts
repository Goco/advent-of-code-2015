const A_CODE = 'a'.charCodeAt(0)
const Z_CODE = 'z'.charCodeAt(0)

export const getMoleculeChain = (molecule: string): string[] =>
  molecule.split('').reduce((acc, char) => {
    const charCode = char.charCodeAt(0)
    if (charCode >= A_CODE && charCode <= Z_CODE) {
      acc[acc.length - 1] += char
    } else {
      acc.push(char)
    }

    return acc
  }, [])
