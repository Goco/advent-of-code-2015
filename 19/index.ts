import { readInput } from '../utils/readInput'
import { getMoleculeChain } from './utils/getMoleculeChain'
import { getRulesRecordFromLines } from './utils/getRulesRecordFromLines'

const MOLECULE =
  'CRnCaSiRnBSiRnFArTiBPTiTiBFArPBCaSiThSiRnTiBPBPMgArCaSiRnTiMgArCaSiThCaSiRnFArRnSiRnFArTiTiBFArCaCaSiRnSiThCaCaSiRnMgArFYSiRnFYCaFArSiThCaSiThPBPTiMgArCaPRnSiAlArPBCaCaSiRnFYSiThCaRnFArArCaCaSiRnPBSiRnFArMgYCaCaCaCaSiThCaCaSiAlArCaCaSiRnPBSiAlArBCaCaCaCaSiThCaPBSiThPBPBCaSiRnFYFArSiThCaSiRnFArBCaCaSiRnFYFArSiThCaPBSiThCaSiRnPMgArRnFArPTiBCaPRnFArCaCaCaCaSiRnCaCaSiRnFYFArFArBCaSiThFArThSiThSiRnTiRnPMgArFArCaSiThCaPBCaSiRnBFArCaCaPRnCaCaPMgArSiRnFYFArCaSiThRnPBPMgAr'

const firstPart = (lines: string[], moleculeChain: string[]) => {
  const rulesRecord = getRulesRecordFromLines(lines)

  const result: Record<string, number> = {}

  moleculeChain.forEach((molecule, index) => {
    const rules = rulesRecord[molecule] ?? []

    rules.forEach((rule) => {
      const newMolecule = [...moleculeChain.slice(0, index), rule, ...moleculeChain.slice(index + 1)].join('')
      const count = result[newMolecule] ?? 0
      result[newMolecule] = count + 1
    })
  })

  return Object.keys(result).length
}

// Shamelessly stolen equation from reddit
const secondPart = (moleculeChain: string[]): number => {
  const totalLength = moleculeChain.length
  const countRnAr = moleculeChain.filter((m) => m === 'Rn' || m === 'Ar').length
  const countY = moleculeChain.filter((m) => m === 'Y').length

  return totalLength - countRnAr - 2 * countY - 1
}

const main = () => {
  const lines = readInput()
  const moleculeChain = getMoleculeChain(MOLECULE)

  const first = firstPart(lines, moleculeChain)
  const second = secondPart(moleculeChain)

  console.log('First part:', first)
  console.log('Second part:', second)
}

main()
