import { readInput } from '../utils/readInput'


type ProcessedLineType = {
  command: string
  from: number[]
  to: number[]
}
const processLines = (lines: string[]): ProcessedLineType[] => {
  return lines.map((line) => {
    const lineParts = line.split(' ')
    const [command, fromRaw, toRaw] =
      lineParts.length === 5 ? [lineParts[1], lineParts[2], lineParts[4]] : [lineParts[0], lineParts[1], lineParts[3]]

    const [from, to] = [fromRaw, toRaw].map((point) => point.split(',').map((n) => +n))

    return { command, from, to }
  })
}

const runPattern = (processedLines: ProcessedLineType[]) => {
  const boolLights: boolean[] = Array(1000000).fill(false)
  const numberLights: number[] = Array(1000000).fill(0)

  processedLines.forEach(({ command, from, to }) => {
    for (let i = from[0]; i <= to[0]; ++i) {
      for (let j = from[1]; j <= to[1]; ++j) {
        switch (command) {
          case 'on':
            boolLights[i * 1000 + j] = true
            numberLights[i * 1000 + j] += 1
            break
          case 'off':
            boolLights[i * 1000 + j] = false
            numberLights[i * 1000 + j] += !!numberLights[i * 1000 + j] ? -1 : 0
            break
          case 'toggle':
            boolLights[i * 1000 + j] = !boolLights[i * 1000 + j]
            numberLights[i * 1000 + j] += 2
            break
          default:
            console.error(`Unknown command: ${command}`)
        }
      }
    }
  })

  return {
    first: boolLights.filter((l) => l).length,
    second: numberLights.reduce((a, b) => a + b, 0),
  }
}

const main = () => {
  const lines = readInput()
  const processedLines = processLines(lines)
  const { first, second } = runPattern(processedLines)

  console.log({ first, second })
}

main()
