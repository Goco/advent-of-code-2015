import { readInput } from '../utils/readInput'

const firstPart = (lines: string[]) => {
  return lines[0].split('').reduce((acc, char) => (char === '(' ? acc + 1 : acc - 1), 0)
}

const secondPart = (lines: string[]) => {
  let floor = 0
  let position = -1

  lines[0].split('').every((char, index) => {
    floor += char === '(' ? 1 : -1

    if (floor === -1) {
      position = index + 1
      return false
    }

    return true
  })

  return position
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
