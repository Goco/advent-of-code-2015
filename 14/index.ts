import { readInput } from '../utils/readInput'

type Reindeer = [string, number, number, number]
type DataRecord = Record<string, number>

const RACE = 2503

const getReindeersFromLines = (lines: string[]): Reindeer[] => {
  return lines.map((line) => {
    const lineSplit = line.split(' ')
    const name = lineSplit[0]
    const speed = +lineSplit[3]
    const move = +lineSplit[6]
    const rest = +lineSplit[13]

    return [name, speed, move, rest]
  })
}

const firstPart = (reindeers: Reindeer[]) => {
  return reindeers
    .map(([, speed, move, rest]) => {
      const cycle = move + rest
      const cycleCount = Math.floor(RACE / cycle)
      const unfinishedCycle = RACE % cycle

      return speed * (move * cycleCount + Math.min(move, unfinishedCycle))
    })
    .sort((a, b) => b - a)[0]
}

const secondPart = (reindeers: Reindeer[]) => {
  const { distance, points }: { distance: DataRecord; points: DataRecord } = reindeers.reduce(
    (acc, [name]) => {
      acc.distance[name] = 0
      acc.points[name] = 0
      return acc
    },
    { distance: {}, points: {} }
  )
  for (let second = 0; second < RACE; ++second) {
    reindeers.forEach(([name, speed, move, rest]) => {
      const cycle = move + rest
      const cyclePart = second % cycle

      if (cyclePart < move) {
        distance[name] += speed
      }
    })

    const distanceEntries = Object.entries(distance).sort(([, d1], [, d2]) => d2 - d1)
    const highestDistance = distanceEntries[0][1]
    distanceEntries.forEach(([name, d]) => {
      if (d === highestDistance) {
        points[name] += 1
      }
    })
  }

  return Object.entries(points).sort(([, d1], [, d2]) => d2 - d1)[0][1]
}

const main = () => {
  const lines = readInput()
  const reindeers = getReindeersFromLines(lines)

  const first = firstPart(reindeers)
  const second = secondPart(reindeers)

  console.log('First part', first)
  console.log('Second part', second)
}

main()
