import { readInput } from '../utils/readInput'


const firstPart = (lines: string[]) => {
  let result = 0

  lines.forEach((line) => {
    result += line.length - eval(line).length
  })

  return result
}

const secondPart = (lines: string[]) => {
  let result = 0

  lines.forEach((line) => {
    result += 2 + line.match(/(")|(\\)/g)!.length
  })

  return result
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
