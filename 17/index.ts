import { readInput } from '../utils/readInput'

const VOLUME = 150

const getContainersFromLines = (lines: string[]): number[] => lines.map((line) => +line)

const generateOptions = (volume: number, containers: number[]): number[][] => {
  return containers.flatMap((container, index) => {
    if (volume === container) {
      return [[container]]
    }
    if (volume > container) {
      return generateOptions(volume - container, containers.slice(index + 1)).map((option) => [container, ...option])
    }

    return []
  })
}

const main = () => {
  const lines = readInput()
  const containers = getContainersFromLines(lines)

  const options = generateOptions(VOLUME, containers)
  const containerCounts = options.map((o) => o.length).sort()

  const first = options.length
  const second = containerCounts.filter((n) => n === containerCounts[0]).length

  console.log('First part:', first)
  console.log('Second part:', second)
}

main()
