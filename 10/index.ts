const main = () => {
  const input = '1321131112'
  let first
  let second

  let line = input
  let i = 0

  while (second === undefined) {
    ++i

    let previousSymbol = line[0]
    let counter = 0
    let newLine = ''

    line.split('').forEach((symbol) => {
      if (previousSymbol === symbol) {
        ++counter
      } else {
        newLine = `${newLine}${counter}${previousSymbol}`
        previousSymbol = symbol
        counter = 1
      }
    })
    newLine = `${newLine}${counter}${previousSymbol}`

    line = newLine

    if (i === 40) {
      first = line.length
    }
    if (i === 50) {
      second = line.length
    }
  }

  console.log({ first, second })
}

main()
