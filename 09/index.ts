import { readInput } from '../utils/readInput'


type GraphType = Record<string, number>
const processData = (lines: string[]) => {
  const destinationsObject: Record<string, boolean> = {}
  const graph: GraphType = {}

  lines.forEach((line) => {
    const [path, distanceRaw] = line.split(' = ')
    const distance = +distanceRaw
    const [from, to] = path.split(' to ')
    destinationsObject[from] = true
    destinationsObject[to] = true
    const key = [from, to].sort().join('|')
    graph[key] = distance
  })

  return {
    destinations: Object.keys(destinationsObject).sort(),
    graph,
  }
}
const generateVariants = (part: string[], destinations: string[]): string[][] => {
  if (destinations.length === 1) {
    return [[...part, destinations[0]]]
  } else {
    return destinations.flatMap((destination) =>
      generateVariants(
        [...part, destination],
        destinations.filter((d) => d !== destination)
      )
    )
  }
}
const computeDistance = (path: string[], graph: GraphType) => {
  let result = 0
  for (let i = 0; i < path.length - 1; ++i) {
    const key = [path[i], path[i + 1]].sort().join('|')
    result += graph[key]!
  }

  return result
}

const main = () => {
  const lines = readInput()

  const { graph, destinations } = processData(lines)
  const allOptions = generateVariants([], destinations)

  const distances = allOptions.map((path) => computeDistance(path, graph))
  const first = Math.min(...distances)
  const second = Math.max(...distances)

  console.log({ first, second })
}

main()
