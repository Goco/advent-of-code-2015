import { readInput } from '../utils/readInput'

const processObject = (obj: object, invalidate?: (obj: object) => boolean): number => {
  if (invalidate?.(obj)) {
    return 0
  }

  let sum = 0
  Object.keys(obj).forEach((key) => {
    const value = obj[key]

    if (typeof value === 'object') {
      sum += processObject(value, invalidate)
    }
    if (typeof value === 'number') {
      sum += value
    }
  })

  return sum
}

const isRedObject = (obj: object) => {
  return Object.entries(obj).some(([key, value]) => value === 'red' && isNaN(+key))
}

const main = () => {
  const [rawJson] = readInput()
  const json = JSON.parse(rawJson)

  const first = processObject(json)
  const second = processObject(json, isRedObject)

  console.log('First part', first)
  console.log('Second part', second)
}

main()
