import { readInput } from '../utils/readInput'
import { getRulesFromLines } from './utils/getRulesFromLines'
import { generateSeatings } from './utils/generateSeatings'
import { getRelationshipRecordFromRules } from './utils/getRelationshipRecordFromRules'
import { getAttendeesFromRules } from './utils/getAttendeesFromRules'
import { countSeating } from './utils/countSeating'

const main = () => {
  const lines = readInput()

  const rules = getRulesFromLines(lines)
  const relationshipRecord = getRelationshipRecordFromRules(rules)

  const attendees = getAttendeesFromRules(rules)

  const seatingsPartOne = generateSeatings(attendees)
  const resultPartOne = seatingsPartOne
    .map((seating) => countSeating(seating, relationshipRecord))
    .sort((a, b) => b - a)[0]

  const seatingsPartTwo = generateSeatings([...attendees, 'Me'])
  const resultPartTwo = seatingsPartTwo
    .map((seating) => countSeating(seating, relationshipRecord))
    .sort((a, b) => b - a)[0]

  console.log('Part one', resultPartOne)
  console.log('Part two', resultPartTwo)
}

main()
