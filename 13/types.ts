export type Rule = [string, string, number]

export type RelationshipRecord = Record<string, Record<string, number>>

export type Relationship = 'gain' | 'lose'
