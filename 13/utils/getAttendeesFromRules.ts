import { Rule } from '../types'

export const getAttendeesFromRules = (rules: Rule[]): string[] =>
  Array.from(
    rules.reduce((acc, rule) => {
      const [a1, a2] = rule
      acc.add(a1)
      acc.add(a2)

      return acc
    }, new Set<string>())
  ).sort()
