import { RelationshipRecord, Rule } from '../types'

export const getRelationshipRecordFromRules = (rules: Rule[]): RelationshipRecord => {
  return rules.reduce((acc, rule) => {
    const [a1, a2, relationship] = rule
    acc[a1] = acc[a1] ?? {}
    acc[a1][a2] = relationship
    return acc
  }, {})
}
