export const generateSeatings = (seating: string[]) => {
  const length = seating.length
  const result = [[...seating]]
  const c = new Array(length).fill(0)
  let i = 1
  let k = 0
  let p = ''

  while (i < length) {
    if (c[i] < i) {
      k = i % 2 && c[i]
      p = seating[i]
      seating[i] = seating[k]
      seating[k] = p
      ++c[i]
      i = 1
      result.push(seating.slice())
    } else {
      c[i] = 0
      ++i
    }
  }
  return result
}
