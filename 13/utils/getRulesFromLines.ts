import { Rule } from '../types'

export const getRulesFromLines = (lines: string[]): Rule[] => {
  return lines.map((line) => {
    const rawRule = line.split(' ')

    const attendee1 = rawRule[0]
    const [attendee2] = rawRule[10].split('.')
    const relationship = (rawRule[2] === 'gain' ? 1 : -1) * +rawRule[3]

    return [attendee1, attendee2, relationship]
  })
}
