import { RelationshipRecord } from '../types'

export const countSeating = (seating: string[], relationshipRecord: RelationshipRecord): number =>
  seating.reduce((acc, a1, i) => {
    const a2 = seating[(i + 1) % seating.length]

    const score1 = relationshipRecord[a1]?.[a2] ?? 0
    const score2 = relationshipRecord[a2]?.[a1] ?? 0

    return acc + score1 + score2
  }, 0)
