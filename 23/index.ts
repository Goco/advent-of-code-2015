import { readInput } from '../utils/readInput'

const runProgram = (lines: string[], register: { a: number; b: number }) => {
  let i = 0
  while (i < lines.length) {
    const [rawCommand, rawOffset] = lines[i].split(', ')
    const [command, variable] = rawCommand.split(' ')

    switch (command) {
      case 'hlf': {
        register[variable] = register[variable] / 2
        i += 1
        break
      }
      case 'tpl': {
        register[variable] *= 3
        i += 1
        break
      }
      case 'inc': {
        register[variable] += 1
        i += 1
        break
      }
      case 'jmp': {
        const offset = +variable
        i += offset
        break
      }
      case 'jie': {
        const offset = +rawOffset
        i += register[variable] % 2 === 0 ? offset : 1
        break
      }
      case 'jio': {
        const offset = +rawOffset
        i += register[variable] === 1 ? offset : 1
        break
      }
      default: {
        console.log({ command, variable, rawOffset })
        throw new Error('OJOJOJ')
      }
    }
  }

  return register.b
}

const main = () => {
  const lines = readInput()

  const first = runProgram(lines, { a: 0, b: 0 })
  const second = runProgram(lines, { a: 1, b: 0 })

  console.log({ first, second })
}

main()
