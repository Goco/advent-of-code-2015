import { readInput } from '../utils/readInput'

const VOWELS = ['a', 'e', 'i', 'o', 'u']
const NAUGHTY_STRINGS = ['ab', 'cd', 'pq', 'xy']

const firstPart = (lines: string[]) => {
  return lines.filter((line) => {
    const chars = line.split('')

    let vowelsCount = 0
    chars.every((char) => {
      if (VOWELS.some((v) => v === char)) {
        vowelsCount += 1
      }

      return vowelsCount <= 3
    })

    const letterTwice = chars.some((char, i) => i > 0 && char === chars[i - 1])
    const withoutNaughtyStrings = NAUGHTY_STRINGS.every((ns) => line.indexOf(ns) === -1)

    return vowelsCount >= 3 && letterTwice && withoutNaughtyStrings
  }).length
}

const secondPart = (lines: string[]) => {
  return lines.filter((line) => {
    const chars = line.split('')

    const map: Record<string, number> = { [`${chars[0]}${chars[1]}`]: 1 }
    let hasTrio = false
    chars.forEach((char, i) => {
      if (i >= 2) {
        const first = chars[i - 2]
        const second = chars[i - 1]
        const third = chars[i]

        const duo = `${second}${third}`
        map[duo] = (map[duo] || 0) + 1

        if (first === third) {
          hasTrio = true
          if (first === second) {
            map[duo] -= 1
          }
        }
      }
    })

    return hasTrio && Object.values(map).some((value) => value > 1)
  }).length
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
