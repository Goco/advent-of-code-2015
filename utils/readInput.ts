import { readFileSync } from 'fs'

export const readInput = (path = './input.txt') => {
  let lines: string[] = []
  try {
    const data = readFileSync(path, 'utf-8')
    lines = data.split(/\r\n|\r|\n/)
    lines.pop()
  } catch (e) {
    console.error(`Error while reading file: ${path}`)
    console.error(e)
  }

  return lines
}
