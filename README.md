# Advent of Code 2015

My solutions for https://adventofcode.com/2015 \
Written in TypeScript.

## How to run the solutions

To run my solutions, you have to had installed `ts-node` in your computer.\
Run `npm install -g ts-node`

Then you have to navigate into the selected day (for example 01) and run the `index.ts` file
```
cd 01
ts-node index.ts
```
