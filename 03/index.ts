import { readInput } from '../utils/readInput'


const firstPart = (lines: string[]) => {
  const map: Record<string, number> = { '0|0': 1 }
  let x = 0
  let y = 0

  lines[0].split('').forEach((move) => {
    switch (move) {
      case '^':
        x += 1
        break
      case 'v':
        x -= 1
        break
      case '>':
        y += 1
        break
      case '<':
        y -= 1
        break
      default:
        console.error('Unknown move: ' + move)
        break
    }
    map[`${x}|${y}`] = (map[`${x}|${y}`] || 0) + 1
  })

  return Object.keys(map).length
}

const secondPart = (lines: string[]) => {
  const map: Record<string, number> = { '0|0': 2 }
  let x = 0
  let y = 0
  let rx = 0
  let ry = 0

  lines[0].split('').forEach((move, i) => {
    const roboMove = !!(i % 2)

    switch (move) {
      case '^': {
        if (roboMove) {
          rx += 1
        } else {
          x += 1
        }
        break
      }
      case 'v': {
        if (roboMove) {
          rx -= 1
        } else {
          x -= 1
        }
        break
      }
      case '>': {
        if (roboMove) {
          ry += 1
        } else {
          y += 1
        }
        break
      }
      case '<': {
        if (roboMove) {
          ry -= 1
        } else {
          y -= 1
        }
        break
      }
      default:
        console.error('Unknown move: ' + move)
        break
    }

    if (roboMove) {
      map[`${rx}|${ry}`] = (map[`${rx}|${ry}`] || 0) + 1
    } else {
      map[`${x}|${y}`] = (map[`${x}|${y}`] || 0) + 1
    }
  })

  return Object.keys(map).length
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
