import { readInput } from '../utils/readInput'


enum OperationEnum {
  NOT = 'NOT',
  AND = 'AND',
  OR = 'OR',
  LSHIFT = 'LSHIFT',
  RSHIFT = 'RSHIFT',
  CONST = 'CONST',
}
type InputType = string | number
type InstructionType = {
  inputs: InputType[]
  output: string
  operation: OperationEnum
}
type VariablesType = Record<string, number>

const uint16 = (n: number) => {
  return n & 0xffff
}

const processLines = (lines: string[]): InstructionType[] => {
  return lines.map((line) => {
    const [leftPart, output] = line.split(' -> ')
    const rawInstructions = leftPart.split(' ')
    const rawInstructionsLength = rawInstructions.length

    if (rawInstructionsLength === 1) {
      const input = rawInstructions[0]
      return {
        output,
        operation: OperationEnum.CONST,
        inputs: [isNaN(+input) ? input : +input],
      }
    }
    if (rawInstructionsLength === 2) {
      const input = rawInstructions[1]
      return {
        output,
        operation: OperationEnum.NOT,
        inputs: [isNaN(+input) ? input : +input],
      }
    }
    const [rawInput1, rawOperation, rawInput2] = rawInstructions
    const operation = rawOperation as unknown as OperationEnum
    const inputs = [rawInput1, rawInput2].map((input) => (isNaN(+input) ? input : +input))

    return { output, operation, inputs }
  })
}

const processInstructions = (instructions: InstructionType[], variables: VariablesType) => {
  instructions.forEach((instruction) => {
    const { output, operation } = instruction
    const inputs = instruction.inputs.map((input) => (typeof input === 'number' ? input : variables[input]))
    if (variables[output] === undefined && inputs.every((input) => input !== undefined)) {
      switch (operation) {
        case OperationEnum.CONST:
          variables[output] = inputs[0]!
          break
        case OperationEnum.NOT:
          variables[output] = uint16(~inputs[0])
          break
        case OperationEnum.AND:
          variables[output] = uint16(inputs[0] & inputs[1])
          break
        case OperationEnum.OR:
          variables[output] = uint16(inputs[0] | inputs[1])
          break
        case OperationEnum.LSHIFT:
          variables[output] = uint16(inputs[0] << inputs[1])
          break
        case OperationEnum.RSHIFT:
          variables[output] = uint16(inputs[0] >> inputs[1])
          break
        default:
      }
    }
  })
}

const main = () => {
  const lines = readInput()
  const instructions = processLines(lines)

  const variables1: VariablesType = {}
  while (variables1['a'] === undefined) {
    processInstructions(instructions, variables1)
  }
  const first = variables1['a']

  instructions.unshift({ inputs: [first], operation: OperationEnum.CONST, output: 'b' })
  const variables2: VariablesType = {}
  while (variables2['a'] === undefined) {
    processInstructions(instructions, variables2)
  }
  const second = variables2['a']

  console.log({ first, second })
}

main()
