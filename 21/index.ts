import { Character, Item, PRICE_ROUNDS } from './types'
import { ARMOR, ATTACK_RINGS, PROTECTION_RINGS, WEAPONS } from './armory'

const BOSS: Character = [104, 8, 1]
const PLAYER: Character = [100, 0, 0]

const countPriceRounds = (items: Item[], rings: Item[], countRounds: (action: number) => number) =>
  items.flatMap((item) =>
    rings.map<PRICE_ROUNDS>((ring) => {
      const price = item[0] + ring[0]
      const action = item[1] + ring[1]
      const rounds = countRounds(action)

      return [price, rounds]
    })
  )

const firstPart = (priceRoundsPlayer: PRICE_ROUNDS[], priceRoundsBoss: PRICE_ROUNDS[]) => {
  const bestPriceRounds = priceRoundsPlayer
    .map(([price, rounds]) => {
      const bossPrice = priceRoundsBoss.find((boss) => boss[1] >= rounds)
      return [price + bossPrice[0], rounds]
    })
    .sort((a, b) => a[0] - b[0])[0]

  return bestPriceRounds[0]
}

const secondPart = (priceRoundsPlayer: PRICE_ROUNDS[], priceRoundsBoss: PRICE_ROUNDS[]) => {
  const bestPriceRounds = priceRoundsPlayer
    .map(([price, rounds]) => {
      const bossPrice = priceRoundsBoss.find((boss) => boss[1] < rounds)
      if (bossPrice) {
        return [price + bossPrice[0], rounds]
      }
    })
    .filter((a) => !!a)
    .sort((a, b) => b[0] - a[0])[0]

  return bestPriceRounds[0]
}

const main = () => {
  const countWeaponRounds = (action: number) => Math.ceil(BOSS[0] / Math.max(1, action - BOSS[2]))
  const priceRoundsPlayer = countPriceRounds(WEAPONS, ATTACK_RINGS, countWeaponRounds)
  const countArmorRounds = (action: number) => Math.ceil(PLAYER[0] / Math.max(1, BOSS[1] - action))
  const priceRoundsBoss = countPriceRounds(ARMOR, PROTECTION_RINGS, countArmorRounds)

  const first = firstPart(
    priceRoundsPlayer.sort((a, b) => a[0] - b[0]),
    priceRoundsBoss.sort((a, b) => a[0] - b[0])
  )

  const second = secondPart(
    priceRoundsPlayer.sort((a, b) => b[0] - a[0]),
    priceRoundsBoss.sort((a, b) => b[0] - a[0])
  )

  console.log({ first, second })
}

main()
