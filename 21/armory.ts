import { Item } from './types'

export const None: Item = [0, 0]

// WEAPONS
export const Dagger: Item = [8, 4]
export const Shortsword: Item = [10, 5]
export const Warhammer: Item = [25, 6]
export const Longsword: Item = [40, 7]
export const Greataxe: Item = [74, 8]

export const WEAPONS = [Dagger, Shortsword, Warhammer, Longsword, Greataxe]

// ARMOR
export const Leather: Item = [13, 1]
export const Chainmail: Item = [31, 2]
export const Splintmail: Item = [53, 3]
export const Bandedmail: Item = [75, 4]
export const Platemail: Item = [102, 5]

export const ARMOR = [None, Leather, Chainmail, Splintmail, Bandedmail, Platemail]

// RINGS
export const Damage1: Item = [25, 1]
export const Damage2: Item = [50, 2]
export const Damage3: Item = [100, 3]
export const Defense1: Item = [20, 1]
export const Defense2: Item = [40, 2]
export const Defense3: Item = [80, 3]

export const ATTACK_RINGS = [None, Damage1, Damage2, Damage3]
export const PROTECTION_RINGS = [None, Defense1, Defense2, Defense3]
