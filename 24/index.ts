import { readInput } from '../utils/readInput'

export const generateOptions = (
  weight: number,
  packages: number[],
  option: number[],
  cache: { size: number }
): number[][] => {
  const optionWeight = option.reduce((a, b) => a + b, 0)

  // Correct option
  if (optionWeight === weight) {
    if (!cache.size || cache.size > option.length) {
      cache.size = option.length
    }
    return [option]
  }

  // Too long option
  if (cache.size && cache.size <= option.length) {
    return []
  }

  // Empty packages
  if (!packages.length) {
    return []
  }

  const [num, ...newPackages] = packages

  return [
    ...(num + optionWeight <= weight ? generateOptions(weight, newPackages, [...option, num], cache) : []),
    ...generateOptions(weight, newPackages, option, cache),
  ]
}

const countQE = (option: number[]): number => option.reduce((a, b) => a * b, 1)

const findSmallestQE = (groupsCount: number, packages: number[]) => {
  const weight = packages.reduce((a, b) => a + b, 0)
  const options = generateOptions(weight / groupsCount, packages, [], { size: 0 }).sort((a, b) => a.length - b.length)
  const smallestOptionLength = options[0].length

  return options
    .filter((o) => o.length === smallestOptionLength)
    .map(countQE)
    .sort((a, b) => a - b)[0]
}

const main = () => {
  const packages = readInput()
    .map((m) => +m)
    .sort((a, b) => b - a)

  const first = findSmallestQE(3, packages)
  const second = findSmallestQE(4, packages)

  console.log({ first, second })
}

main()
