const md5 = require('md5')
const input = 'ckczppom'

const findSalt = (zeroCount: number) => {
  const condition = Array(zeroCount).fill('0').join('')
  let result = 0
  while (true) {
    const hash = md5(`${input}${result}`)

    if (hash.startsWith(condition)) {
      break
    }
    result += 1
  }

  return result
}

const main = () => {
  const first = findSalt(5)
  const second = findSalt(6)

  console.log({ first, second })
}

main()
