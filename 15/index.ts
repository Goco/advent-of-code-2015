import { readInput } from '../utils/readInput'
import { getIngredientsFromLines } from './utils/getIngredientsFromLines'
import { generateOptions } from './utils/generateOptions'
import { TEASPOON_COUNT } from './constants'
import { countOption } from './utils/countOption'
import { countProperty } from './utils/countProperty'

const main = () => {
  const lines = readInput()
  const ingredients = getIngredientsFromLines(lines)
  const options = generateOptions(TEASPOON_COUNT, ingredients.length)

  const countedOptions = options
    .map<[number, number[]]>((option) => [countOption(option, ingredients), option])
    .sort((a, b) => b[0] - a[0])

  const first = countedOptions[0][0]
  console.log('First part:', first)

  const second = countedOptions.find(([, option]) => countProperty(option, ingredients, 'calories') === 500)?.[0]
  console.log('Second part:', second)
}

main()
