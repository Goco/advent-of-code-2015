export type Property = 'capacity' | 'durability' | 'flavor' | 'texture' | 'calories'

export type Ingredient = [string, Record<Property, number>]
