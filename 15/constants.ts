import { Property } from './types'

export const TEASPOON_COUNT = 100
export const PROPERTIES: Property[] = ['capacity', 'durability', 'flavor', 'texture']
