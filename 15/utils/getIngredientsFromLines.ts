import { Ingredient, Property } from '../types'

export const getIngredientsFromLines = (lines: string[]): Ingredient[] =>
  lines.map((line) => {
    const [ingredient, rawProperties] = line.split(': ')

    const properties = rawProperties.split(', ').reduce((acc, rawProp) => {
      const splitProp = rawProp.split(' ')
      const property = splitProp[0] as Property
      acc[property] = +splitProp[1]

      return acc
    }, {}) as Record<Property, number>

    return [ingredient, properties]
  })
