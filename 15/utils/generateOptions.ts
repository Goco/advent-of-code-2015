export const generateOptions = (volume: number, count: number): number[][] => {
  if (count === 1) {
    return [[volume]]
  }

  return Array.from({ length: volume + 1 }).flatMap((_, i) =>
    generateOptions(volume - i, count - 1).map((option) => [i, ...option])
  )
}
