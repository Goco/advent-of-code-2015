import { Ingredient, Property } from '../types'

export const countProperty = (option: number[], ingredients: Ingredient[], property: Property) =>
  ingredients.map((ingredient, i) => option[i] * ingredient[1][property]).reduce((a, b) => a + b, 0)
