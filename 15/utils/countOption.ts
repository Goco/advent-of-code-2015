import { Ingredient } from '../types'
import { PROPERTIES } from '../constants'
import { countProperty } from './countProperty'

export const countOption = (option: number[], ingredients: Ingredient[]): number =>
  PROPERTIES.map((property) => Math.max(0, countProperty(option, ingredients, property))).reduce((a, b) => a * b, 1)
