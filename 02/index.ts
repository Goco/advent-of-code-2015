import { readInput } from '../utils/readInput'


const firstPart = (lines: string[]) => {
  let result = 0
  lines.forEach((line) => {
    const [l, w, h] = line.split('x').map((value) => +value)
    const lw = l * w
    const wh = w * h
    const hl = h * l

    result += 2 * (lw + wh + hl) + Math.min(lw, wh, hl)
  })
  return result
}

const secondPart = (lines: string[]) => {
  let result = 0
  lines.forEach((line) => {
    const [l, w, h] = line.split('x').map((value) => +value)
    const wrap = 2 * Math.min(l + w, w + h, h + l)
    const ribbon = l * w * h

    result += wrap + ribbon
  })
  return result
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
