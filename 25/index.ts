// row 2978, column 3083.
const ROW = 2978
const COLUMN = 3083

const FIRST_NUMBER = 20151125

const main = () => {
  let code = FIRST_NUMBER

  let i = 0
  let result = 0

  while (!result) {
    ++i
    for (let j = 0; j < i; ++j) {
      const row = i - j
      const column = 1 + j

      if (row === ROW && column === COLUMN) {
        result = code
        break
      }
      code = (code * 252533) % 33554393
    }
  }

  console.log(code)
}

main()
