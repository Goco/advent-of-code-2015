import { readInput } from '../utils/readInput'

const TAPE = {
  children: 3,
  cats: 7,
  samoyeds: 2,
  pomeranians: 3,
  akitas: 0,
  vizslas: 0,
  goldfish: 5,
  trees: 3,
  cars: 2,
  perfumes: 1,
}

type Aunt = [string, [string, number][]]

const getAuntsFromLines = (lines: string[]): Aunt[] =>
  lines.map((line) => {
    const [name, ...rawData] = line.split(/[:,] /)
    const data: [string, number][] = []
    for (let i = 0; i < rawData.length / 2; ++i) {
      const attribute = rawData[2 * i]
      const value = +rawData[2 * i + 1]
      data.push([attribute, value])
    }

    return [name, data]
  })

const firstPart = (aunts: Aunt[]) => {
  return aunts.find((aunt) => {
    return aunt[1].every(([attribute, value]) => {
      const tapeValue = TAPE[attribute]

      return value === tapeValue
    })
  })
}

const secondPart = (aunts: Aunt[]) => {
  return aunts.find((aunt) => {
    return aunt[1].every(([attribute, value]) => {
      const tapeValue = TAPE[attribute]

      if (['cats', 'trees'].some((a) => a === attribute)) {
        return value > tapeValue
      }

      if (['pomeranians', 'goldfish'].some((a) => a === attribute)) {
        return value < tapeValue
      }
      return value === tapeValue
    })
  })
}

const main = () => {
  const lines = readInput()
  const aunts = getAuntsFromLines(lines)

  const first = firstPart(aunts)[0]
  const second = secondPart(aunts)[0]

  console.log('First part:', first)
  console.log('Second part:', second)
}

main()
