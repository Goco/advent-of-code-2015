import { A_CODE } from '../constants'

export const passwordToCharCodes = (password: string): number[] =>
  password.split('').map((l) => l.charCodeAt(0) - A_CODE)
