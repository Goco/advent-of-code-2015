import { passwordToCharCodes } from './passwordToCharCodes'
import { A_CODE } from '../constants'

const I = 'i'.charCodeAt(0) - A_CODE
const O = 'o'.charCodeAt(0) - A_CODE
const L = 'l'.charCodeAt(0) - A_CODE

const PROHIBITED = [I, O, L]

export const checkPassword = (password: string): boolean => {
  const charCodes = passwordToCharCodes(password)

  const { first, third } = charCodes.reduce(
    (acc, code, index) => {
      if (index > 1 && !acc.first) {
        const first = charCodes[index - 2] + 2
        const second = charCodes[index - 1] + 1

        acc.first = code === first && code === second
      }
      if (index > 0) {
        const first = index > 1 ? charCodes[index - 2] : NaN
        const second = charCodes[index - 1]

        if (code === second && code !== first) {
          acc.third += 1
        }
      }

      return acc
    },
    { first: false, third: 0 }
  )

  const firstRequirement = first
  const secondRequirement = charCodes.every((c) => PROHIBITED.every((p) => p !== c))
  const thirdRequirement = third >= 2

  return firstRequirement && secondRequirement && thirdRequirement
}
