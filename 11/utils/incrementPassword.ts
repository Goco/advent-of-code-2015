import { A_CODE, SUM_LETTERS } from '../constants'
import { passwordToCharCodes } from './passwordToCharCodes'

export const incrementPassword = (password: string): string => {
  const charCodes = passwordToCharCodes(password).reverse()

  let overflow = 1
  return charCodes
    .map((code) => {
      code += overflow
      overflow = 0

      if (code === SUM_LETTERS) {
        overflow = 1
      }

      return String.fromCharCode((code % SUM_LETTERS) + A_CODE)
    })
    .reverse()
    .join('')
}
