import { checkPassword } from './utils/checkPassword'
import { incrementPassword } from './utils/incrementPassword'

const INPUT = 'cqjxjnds'

const main = () => {
  let newPass = INPUT
  let correctPass = false

  while (!correctPass) {
    newPass = incrementPassword(newPass)
    correctPass = checkPassword(newPass)
  }
  console.log('First part', newPass)

  correctPass = false
  while (!correctPass) {
    newPass = incrementPassword(newPass)
    correctPass = checkPassword(newPass)
  }

  console.log('Second part', newPass)
}

main()
